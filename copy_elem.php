<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Перенос");

if (CModule::IncludeModule("iblock")):
CModule::IncludeModule("catalog");
$oldSect = $_GET["old_sect"];
$newSect = $_GET["new_sect"];
   $rs = CIBlockElement::GetList (
        false,
        Array("IBLOCK_ID" => 22, "SECTION_ID" => $oldSect,"ACTIVE" => "Y"),
        false,
        false,
        Array('ID', 'NAME', 'ACTIVE_FROM', "CODE", "PROPERTY_PRICE","PREVIEW_PICTURE","PROPERTY_BRAND_REF","PROPERTY_ARTNUMBER","PROPERTY_COUNTRY","PROPERTY_INSTRUMENT_TYPE") //набор свойств меняем на свой
    );

    while ($ar_fields = $rs->GetNext())
    {

        echo "<pre>";
        print_r($ar_fields);
        echo "</pre>";
       // echo $ar_fields["PROPERTY_PRICE_VALUE"];

        $el = new CIBlockElement;
        $prevPict = CFile::GetPath($ar_fields["PREVIEW_PICTURE"]);

        $arProps = [
            "ARTNUMBER" => $ar_fields["PROPERTY_ARTNUMBER_VALUE"],
            "BRAND_REF"=> $ar_fields["PROPERTY_BRAND_REF_VALUE"],
            "COUNTRY"=> $ar_fields["PROPERTY_COUNTRY_VALUE"],
            "TYPE"=> $ar_fields["PROPERTY_INSTRUMENT_TYPE_VALUE"],
        ]; //в массиве $arProps свойства меняем на свои
        $arLoadProductArray = Array(
          "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
          "IBLOCK_SECTION_ID" => $newSect,          // элемент лежит в корне раздела
          "IBLOCK_ID"      => 4,
          "DATE_ACTIVE_FROM" => $ar_fields['ACTIVE_FROM'],
          "NAME"           => $ar_fields['NAME'],
          "ACTIVE"         => "Y",  
          "CODE" =>  $ar_fields["CODE"],  
          'PROPERTY_VALUES' => $arProps,
          "PREVIEW_PICTURE" => CFile::MakeFileArray($prevPict),
          );

        if($PRODUCT_ID = $el->Add($arLoadProductArray)){
            echo "New ID: ".$PRODUCT_ID;
            $PRICE_TYPE_ID = 1;

            $arFields = Array(
                "PRODUCT_ID" => $PRODUCT_ID,
                "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                "PRICE" => $ar_fields["PROPERTY_PRICE_VALUE"],
                "CURRENCY" => "RUB",
                "QUANTITY_FROM" => 1,
                "QUANTITY_TO" => 10
            );

            $res = CPrice::GetList(
                    array(),
                    array(
                            "PRODUCT_ID" => $PRODUCT_ID,
                            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
                        )
                );

            if ($arr = $res->Fetch())
            {
                CPrice::Update($arr["ID"], $arFields);
            }
            else
            {
                CPrice::Add($arFields);
            }  
        }else{
          echo "Error: ".$el->LAST_ERROR;
        }


    }
endif;

 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>